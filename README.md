# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://gitlab.com/Rohatgi/thermometer-application).

## Available Scripts

In the project directory, you can run:
Before running make sure you install all dependency mention in package.json file like axios and recharts
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Running test cases
1. npm install --save-dev @testing-library/react @testing-library/jest-dom jest axios
2. run command npm run test 
Note- change the filename while mocking . I tested with my File

EditorUsed-visualstudio
Note- I have consider all datasets for show trend chart for Temperature Report