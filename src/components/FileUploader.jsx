// FileUploader.jsx

import React, { useState } from 'react';
import axios from 'axios';


const FileUploader = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadStatus, setUploadStatus] = useState(null);


  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);
  };

  const handleFileUpload = async () => {
    try {
      const formData = new FormData();
      formData.append('thermFile', selectedFile);

      setUploadStatus('Uploading File... Please wait it is uploading.. We can check DB while uploading..It takes 15-20min or more to upload 300-500MB data');

      const response = await axios.post('http://localhost:3001/api/thermometer/upload', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      setUploadStatus('File uploaded successfully');
      console.log('File uploaded successfully', response.data);

      
      
    } catch (error) {
      setUploadStatus('Error uploading file');
      console.error('Error uploading file:', error.message);
    }
  };


  return (
    <div>
      <input type="file" name="thermFile" onChange={handleFileChange} />
      <button onClick={handleFileUpload}>Upload</button>
      {uploadStatus && <p>{uploadStatus}</p>}

    </div>
  );
};

export default FileUploader;
