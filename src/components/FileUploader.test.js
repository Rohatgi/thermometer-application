// FileUploader.test.jsx
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import FileUploader from './FileUploader';

describe('FileUploader', () => {
  it('renders FileUploader component', () => {
    render(<FileUploader />);
    const fileInput = screen.getByLabelText('Upload File');
    expect(fileInput).toBeInTheDocument();
  });

  it('handles file change and upload', async () => {
    const mockOnUpload = jest.fn();
    render(<FileUploader onUpload={mockOnUpload} />);
    const fileInput = screen.getByLabelText('Upload File');

    // Mock a file selection
    const file = new File(['(content)'], 'test-file.json', { type: 'application/json' });
    fireEvent.change(fileInput, { target: { files: [file] } });

    // Mock the file upload
    const uploadButton = screen.getByText('Upload');
    fireEvent.click(uploadButton);

    // Ensure the onUpload callback is called
    expect(mockOnUpload).toHaveBeenCalledTimes(1);
  });
});
