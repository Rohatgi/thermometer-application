// TemperatureChart.jsx
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,Brush } from 'recharts';

const TemperatureChart = () => {
  const [temperatureData, setTemperatureData] = useState([]);

  useEffect(() => {
    // Fetch temperature history data
    const fetchTemperatureData = async () => {
      try {

        const response = await axios.get('http://localhost:3001/api/thermometer/history');

        setTemperatureData(response.data);
      } catch (error) {
        console.error('Error fetching temperature data:', error.message);
      }
    };

    fetchTemperatureData();
  }, []); // Empty dependency array to run the effect only once
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString(); // Customize the date format if needed
  };
  return (
    <div>
      <h2>Temperature Trend Chart</h2>
      <LineChart width={800} height={400} data={temperatureData}>
        <CartesianGrid stroke="#ccc" />
        <XAxis dataKey="datetime" tickFormatter={formatDate} />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="temperature" stroke="rgb(75, 192, 192)" />
        <Brush dataKey="datetime" height={30} stroke="#8884d8" />
      </LineChart>
    </div>
  );
};


export default TemperatureChart;
