// App.jsx
import React, { useState } from 'react';
import FileUploader from './components/FileUploader';
import TemperatureChart from './components/TemperatureChart';

const App = () => {
  const [uploadTrigger, setUploadTrigger] = useState(0);

  const handleUpload = () => {
    // Increment the trigger to force a re-fetch of temperature data after upload
    setUploadTrigger((prev) => prev + 1);
  };

  return (
    <div>
      <h1>Thermometer App</h1>
      <FileUploader onUpload={handleUpload} />
      <TemperatureChart key={uploadTrigger} />
    </div>
  );
};

export default App;
